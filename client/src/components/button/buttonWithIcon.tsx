import './style.less'

import { Text, Button } from '@tarojs/components'

type Props = {
  className: string
  icon?: string
  title: string
}

export function ButtonWithIcon(props: Props) {
  return (
    <Button className={props.className}>
      <Text>{props.title}</Text>
    </Button>
  )
}
